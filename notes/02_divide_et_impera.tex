\section{Divide et impera}
\par A volte può essere utile risolvere un problema scomponendolo ricorsivamente (\textit{divide}) in piccole parti la cui soluzione è facilmente determinabile, per poi unirle (\textit{impera}) a formare la soluzione. Un esempio di problemi di risolvibili tramite divide et impera sono quelli di ordinamento.
\subsection{Merge sort}
\par Il merge sort prevede di scomporre una lista di numeri in parti bilanciate fino a quando esse non contengonno un solo elemento. Dopodiché si procede a unire le due parti in modo che gli elementi siano ordinati.
\par Un esempio di codice, reperibile su https://gitlab.com/eutampieri/algorithms, è il seguente.
\begin{minted}{rust}
fn merge_sort<T: Ord + Clone>(list: &[T]) -> Vec<T> {
    if list.len() == 1 {
        // Un array di un elemento è già ordinato (caso base)
        return Vec::from(list);
    }
    let middle_point = list.len()/2;
    merge(
        merge_sort(&list[0 .. middle_point]),
        merge_sort(&list[middle_point..])
    )
}
\end{minted}
\par Nel blocco di codice sopra, il passo ricorsivo è rappresentato dalla chiamata alla funzione $merge$ (linea 6), che deve unire le due metà ordinate ottenute alle linee 7 e 8.
\par La funzione $merge$ si occupa di inserire gli elementi delle due metà nell'ordine giusto:
\begin{minted}{rust}
fn merge<T: Ord + Clone>(left: Vec<T>, right: Vec<T>) -> Vec<T> {
    // Il vettore che conterrà il risultato ordinato
    let mut res: Vec<T> = Vec::new();
    let mut left_iterator = left.iter();
    let mut right_iterator = right.iter();

    let mut l = left_iterator.next();
    let mut r = right_iterator.next();
    loop {
        // Se abbiamo visitato tutti gli elementi dei due array abbiamo finito
        if l.is_none() && r.is_none() {
            break;
        } else if l.is_none() {
            // Se uno dei due array è finito prendiamo solo gli
            // elementi dell'altro array
            res.push(r.unwrap().clone());
            r = right_iterator.next();
        } else if r.is_none() {
            res.push(l.unwrap().clone());
            l = left_iterator.next()
        } else {
            // Altrimenti dobbiamo scegliere quale elemento inserire
            let left_val = l.unwrap();
            let right_val = r.unwrap();
            if left_val < right_val {
                res.push(left_val.clone());
                l = left_iterator.next();
            } else {
                res.push(right_val.clone());
                r = right_iterator.next();
            }
        }
    }
    res
}
\end{minted}
\subsubsection{Costo computazionale}
\par L'unione dei due sottovettori avviene in tempo lineare ($\Theta(n)$), perché dobbiamo scorrere due vettori.
\par Il costo computazionale dell'intero algoritmo va rappresentato con un'equazione ricorsiva:
\begin{equation*}
    T(n) = \begin{cases}
        \Theta(1) & n=1 \\
        2T(\frac{n}{2})+\Theta(n) & n > 1
    \end{cases}
\end{equation*}
\par Se la lunghezza dell'array da ordinare è 1 ($n=1$), l'array è già ordinato quindi non eseguiamo operazioni quindi l'algoritmo conclude in tempo costante.
\par Se invece $n>1$, il costo computazionale è rappresentato dal due volte il costo dell'ordinamento sulle due metà dell'array ($2T(\frac{n}{2})$) più il costo di unire le due metà, che è lineare ($\Theta(n)$).
\par Risolvendo l'equazione ricorsiva otteniamo che il tempo di esecuzione dell'algoritmo è $T(n) = \Theta(n\log(n))$ nel caso ottimo, nel caso medio e nel caso pessimo.
%
%
\subsection{Quicksort}
\includegraphics[width=\textwidth]{notes/quick-sort}
\par Fondamentale per Quicksort è la procedura \texttt{partition}, che si occupa di spostare gli elementi rispetto al pivot.
\begin{minted}{swift}
func partition<T: Comparable>(_ array: inout [T], range: ClosedRange<Int>)
    -> (ClosedRange<Int>, ClosedRange<Int>)
{
    let pivot = array[range.upperBound]
    // Variabile che memorizza dove siamo arrivati a fare la partizione
    var endThanLessOfPivot = range.lowerBound - 1
    for i in range.dropLast() {
        if array[i] <= pivot {
            endThanLessOfPivot += 1
            array.swapAt(endThanLessOfPivot, i)
        }
    }
    array.swapAt(endThanLessOfPivot + 1, range.upperBound)
    // Ritorno i range dei due sottoarray
    return (
        range.lowerBound...max(endThanLessOfPivot, range.lowerBound),
        min(endThanLessOfPivot + 2, range.upperBound)...range.upperBound
    )
}
\end{minted}
\par Sappiamo che il pivot si trova già nella sua posizione finale, quindi chiamiamo \texttt{partition} sui due sottointervalli:
\begin{minted}{swift}
func quicksort<T: Comparable>(_ array: inout [T], range: ClosedRange<Int>) {
    if range.count == 1 { // Caso base
        return
    }
    let subranges = partition(&array, range: range)
    quicksort(&array, range: subranges.0)
    quicksort(&array, range: subranges.1)
}
\end{minted}
\par Potrebbe capitare di ottenere una partizione sbilanciata, ad esempio se l'elemento in ultima posizione è il massimo. Può essere utile quindi effettuare una partizione scegliendo il pivot casualmente.
\begin{minted}{swift}
func randomPartition<T: Comparable>(_ array: inout [T], range: ClosedRange<Int>)
-> (ClosedRange<Int>, ClosedRange<Int>)
{
    let pivot = range.randomElement()!
    array.swapAt(range.upperBound, pivot)
    return partition(&array, range: range)
}
\end{minted}
\subsubsection{Costo computazionale}
\par Nel caso in cui le partizioni siano bilanciate abbiamo
\begin{equation*}
    T(n) = \begin{cases}
        \Theta(1)&n=1\\
        2T(n/2) + \Theta(n/2) &n>1
    \end{cases} = \Theta(n\log(n))
\end{equation*}
\par Nel caso invece in cui le partizioni siano sbilanciate abbiamo
\begin{equation*}
    T(n) = \begin{cases}
        \Theta(1)&n=1\\
        2T(n/2) + \Theta(n-1) &n>1
    \end{cases} = \Theta(n^2)
\end{equation*}
\par Possiamo pensare che il costo del quicksort randomizzato sia $\Theta(n\log(n))$ perché avere una partizione sbilanciata è equiprobabile rispetto ad avere una partizione bilanciata.
