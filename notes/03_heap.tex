\section{Heap}
\par Gli heap sono delle strutture dati ad albero binario completo in tutti i livelli, tranne al più l'ultimo, utilizzata per memorizzare chiavi. Possono essere di vari tipi.
\par Essendo un albero binario, un heap ha altezza $h=\Theta(\log(n))$.
\par L'heap viene memorizzato in un vettore, nel quale il primo elemento è la radice e i successivi elementi sono i nodi da sinistra a destra dei vari livelli. Ciò significa che, dato un nodo in posizione $i$, il suo figlio destro è situato in $2i$, quello sinistro in $2i+1$ e il padre è situato in $i/2$. Questo significa che la navigazione avviene in tempo lineare ($\Theta(1)$).
\par Un'implementazione di un heap generico
\begin{minted}{swift}
enum HeapError: Error {
    case NewValueError
    case HeapUnderflow
}

class Node<Comparable> {
    internal
    weak var heap: Heap<Comparable>!
    let i: Int
    public
    init(heap: Heap<Comparable>, position: Int) {
        self.heap = heap
        self.i = position
    }
    var value: Comparable? {
        get {
            if let heap = heap {
                return heap.storage[i]
            }
            return nil
        }
    }
    var parent: Node<Comparable>? {
        if i == 0 {
            return nil
        }
        return Node(heap: heap, position: i/2)
    }
    var left: Node<Comparable>? {
        if heap == nil {
            return nil
        }
        if 2*i + 1 >= heap.storage.count {
            return nil
        }
        return Node(heap: heap, position: 2*i + 1)
    }
    var right: Node<Comparable>? {
        if heap == nil {
            return nil
        }
        if 2 * i + 2 >= heap.storage.count {
            return nil
        }
        return Node(heap: heap, position: 2 * i + 2)
    }
}
class Heap<Comparable> {
    internal
    var storage: [Comparable]
    public
    init() {
        self.storage = []
    }
    init(from array: [Comparable]) {
        self.storage = array
    }
    var root: Node<Comparable> {
        return Node(heap: self, position: 0)
    }
    var empty: Bool {
        get {
            self.storage.count == 0
        }
    }
}    
\end{minted}
\subsection{Max-heap}
\par Un max heap viene usato per trovare il valore massimo in un insieme di elementi. I figli di ogni nodo sono minori o uguali al loro padre e nella radice è contenuto il valore massimo.
\begin{minted}{swift}
class MaxHeap<T: Comparable>: Heap<T>{
    public
    var max: T {
        return self.root.value!
    }
    override init(from array: [T]) {
        super.init(from: array)
        let nodes = (0..<self.storage.count).reversed().map {
            Node<T>(heap: self, position: $0)
        }
        for n in nodes {
            self.maxHeapify(node: n)
        }
    }
    func maxHeapify() {
        self.maxHeapify(node: self.root)
    }
    func maxHeapify(node: Node<T>){
        let l = node.left
        let r = node.right
        var largest = node
        if let l = l {
            if l.value! > node.value! {
                largest = l
            }
        }
        if let r = r {
            if r.value! > largest.value! {
                largest = r
            }
        }
        if largest.i != node.i {
            self.storage.swapAt(largest.i, node.i)
            self.maxHeapify(node: largest)
        }
    }
    func increaseKey(at: Node<T>, value: T) throws {
        if value < at.value!{
            throw HeapError.NewValueError
        }
        self.storage[at.i] = value
        var currentNode: Node<T>? = at
        while currentNode != nil && self.storage[currentNode!.parent!.i] <
            self.storage[currentNode!.i] {
            self.storage.swapAt(currentNode!.parent!.i, currentNode!.i)
            currentNode = currentNode?.parent
        }
    }
    func insert(key: T) throws {
        self.storage.append(key)
        try self.increaseKey(
            at: Node(heap: self, position: self.storage.count - 1),
            value: key
        )
    }
    func pop() throws -> T {
        if self.storage.count == 0 {
            throw HeapError.HeapUnderflow
        } else if self.storage.count == 1 {
            return self.storage.popLast()!
        }
        let max = self.max
        self.storage[0] = self.storage.popLast()!
        self.maxHeapify()
        return max
    }
}
\end{minted}
\subsubsection{Costo computazionale}
\begin{description}
    \item[Navigazione] $\Theta(1)$
    \item[\texttt{maxHeapify}] $\Theta(h) = \Theta(\log(n))$
    \item[Inizializzazione] $\Theta(n)$
    \item[Ricerca del massimo] $\Theta(1)$
    \item[Incremento di una chiave] $\Theta(\log(n))$
    \item[Inserimento di una chiave] $\Theta(\log(n))$
    \item[Estrazione del massimo] $\Theta(\log(n))$
\end{description}
\subsection{Min-heap}
\par Un min heap viene usato per trovare il valore minimo in un insieme di elementi. I figli di ogni nodo sono maggiori o uguali al loro padre e nella radice è contenuto il valore minimo.
\begin{minted}{swift}
class MinHeap<T: Comparable>: Heap<T>{
    public
    var min: T {
        return self.root.value!
    }
    override init(from array: [T]) {
        super.init(from: array)
        let nodes = (0..<self.storage.count).reversed().map {
            Node<T>(heap: self, position: $0)
        }
        for n in nodes {
            self.minHeapify(node: n)
        }
    }
    func minHeapify() {
        self.minHeapify(node: self.root)
    }
    func minHeapify(node: Node<T>){
        let l = node.left
        let r = node.right
        var smallest = node
        if let l = l {
            if l.value! < node.value! {
                smallest = l
            }
        }
        if let r = r {
            if r.value! < smallest.value! {
                smallest = r
            }
        }
        if smallest.i != node.i {
            self.storage.swapAt(smallest.i, node.i)
            self.minHeapify(node: smallest)
        }
    }
    func decreaseKey(at: Node<T>, value: T) throws {
        if value > at.value!{
            throw HeapError.NewValueError
        }
        self.storage[at.i] = value
        var currentNode: Node<T>? = at
        while currentNode != nil && self.storage[currentNode!.parent!.i] 
            > self.storage[currentNode!.i] {
            self.storage.swapAt(currentNode!.parent!.i, currentNode!.i)
            currentNode = currentNode?.parent
        }
    }
    func insert(key: T) throws {
        self.storage.append(key)
        try self
            .decreaseKey(at: Node(heap: self, position: self.storage.count - 1),
                value: key
            )
    }
    func pop() throws -> T {
        if self.empty {
            throw HeapError.HeapUnderflow
        } else if self.storage.count == 1 {
            return self.storage.popLast()!
        }
        let min = self.min
        self.storage[0] = self.storage.popLast()!
        self.minHeapify()
        return min
    }
}
\end{minted}
\subsubsection{Costo computazionale}
\begin{description}
    \item[Navigazione] $\Theta(1)$
    \item[\texttt{minHeapify}] $\Theta(h) = \Theta(\log(n))$
    \item[Inizializzazione] $\Theta(n)$
    \item[Ricerca del minimo] $\Theta(1)$
    \item[Decremento di una chiave] $\Theta(\log(n))$
    \item[Inserimento di una chiave] $\Theta(\log(n))$
    \item[Estrazione del minimo] $\Theta(\log(n))$
\end{description}
\subsection{Heapsort}
\par Tramite un heap è possibile ordinare un array in place. Segue un'implementazione non in place.
\begin{minted}{swift}
public func heapsort<T: Comparable>(array: inout [T]) {
    let minHeap = MinHeap(from: array)
    var newArray: [T] = []
    while !minHeap.empty {
        do {
            try newArray.append(minHeap.pop())
        } catch {
            print("ERR")
            return
        }
    }
    array = newArray
}
\end{minted}
\section{Priority queues}
\par Le code a priorità possono essere implementate tramite vettori disordinati, vettori ordinati oppure heap. Nel caso in ci vengano implementate con gli heap il massimo si trova in $\Theta(1)$,  si estrae in $\Theta(\log(n))$ e un nuovo elemento si inserisce in $\Theta(\log(n))$.
